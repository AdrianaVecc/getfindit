package com.adrianavecchioli.findit;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.speech.RecognizerIntent;

import com.adrianavecchioli.findit.provider.AndroidLocationProvider;
import com.adrianavecchioli.findit.provider.LocationChangedListener;
import com.adrianavecchioli.findit.util.RememberUtils;

public class BaseActivity extends Activity implements LocationChangedListener {


	private AndroidLocationProvider locationProvider=null;
	protected Location lastLocation;
	protected String tag;
	protected Intent reLauncIntent;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.reLauncIntent=getIntent();
		RememberUtils.startSynchronization(this);
		tag = extractTagFromUserVoices();
		String exitAppCmd=this.getString(R.string.cmd_exit_app);
		if(exitAppCmd.equalsIgnoreCase(tag)){
			finish();
			return;
		}
		locationProvider=new AndroidLocationProvider(this, this);
		locationProvider.startLocationService();
		lastLocation=locationProvider.getLastLocation();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		locationProvider.stopLocationService();
	}
	private String extractTagFromUserVoices() {
		String tag = null;
		ArrayList<String> voiceResults = getIntent().getExtras()
				.getStringArrayList(RecognizerIntent.EXTRA_RESULTS);
		if (voiceResults != null && !voiceResults.isEmpty()) {
			tag = voiceResults.get(0);
			
		}
		return tag;
	}
	@Override
	public void onLocationReceived(Location location) {
		this.lastLocation=location;
		
	}
}
