package com.adrianavecchioli.findit;

import java.util.ArrayList;
import java.util.List;

import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;

import com.adrianavecchioli.findit.adapter.ScrollAdapter;
import com.adrianavecchioli.findit.db.SqlHelper;
import com.adrianavecchioli.findit.domain.RememberItem;
import com.adrianavecchioli.findit.util.RememberUtils;
import com.google.android.glass.app.Card;
import com.google.android.glass.app.Card.ImageLayout;
import com.google.android.glass.widget.CardScrollView;
import com.parse.ParseGeoPoint;

public class Find extends BaseActivity implements Callback{

	private static final int DELETE = 0x1212;
	private static final int FAILURE = 0x1009;
	private RememberItem itemSelected;
	private TextView mCardTitle;
	private TextView mCardSubTitle;
	private ImageView mCardImage;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		List<RememberItem> allItems = SqlHelper.getInstance(this)
				.findAllRememberItem();
		if(allItems.isEmpty()){
			showEmptyCard();
			return;
		}
		List<RememberItem> items=findRememberItemByTag(allItems,tag);
		displayRememberItems(items);
		
	}
	private List<RememberItem> findRememberItemByTag(List<RememberItem>items, String tag){
		String everyThingTag=this.getString(R.string.find_all_thing);
		if(everyThingTag.equalsIgnoreCase(tag)){
			return items;
		}
		List<RememberItem> list=new ArrayList<RememberItem>();
		for(RememberItem item:items){
			if(item.getTag().toLowerCase().contains(tag.toLowerCase())){
				list.add(item);
			}
			
		}
		return list;
		
	}
	private void displayFailureView() {
		setContentView(R.layout.card_full_image);
		mCardTitle = (TextView) findViewById(R.id.card_title);
		mCardSubTitle = (TextView) findViewById(R.id.card_subtitle);
		mCardImage = (ImageView) findViewById(R.id.card_image);
		mCardImage.setScaleType(ScaleType.CENTER_INSIDE);	
		mCardTitle.setText(R.string.storefailhead);
		mCardSubTitle.setText(R.string.storefailfoot);
		mCardImage.setImageResource(R.drawable.finditlogobg);
		mCardTitle.setGravity(Gravity.CENTER);
		mCardSubTitle.setGravity(Gravity.CENTER);
		
		Handler handler = new Handler(this);
		handler.sendEmptyMessageDelayed(FAILURE, 2000);
	}
	private Card createCardOfRememberItem(RememberItem item) {
		Card card = new Card(this);
		card.setText(item.getTag());
		card.setFootnote(String.format(item.getGeoAddress()+" - "+"%tA %tB %td", item.getAddedDate(),item.getAddedDate(),item.getAddedDate()));
		card.setImageLayout(ImageLayout.FULL);
		card.addImage(BitmapFactory.decodeFile(item.getImagePath()));
		return card;
	}


	private void displayRememberItems(final List<RememberItem> items) {
		if(items==null || items.isEmpty()){
			displayFailureView();
			return;
		}
		List<Card> mCards = new ArrayList<Card>();
		for (RememberItem item : items) {
			Card card = createCardOfRememberItem(item);
			mCards.add(card);
		}
		CardScrollView mCardScrollView = new CardScrollView(this);
		final ScrollAdapter adapter = new ScrollAdapter(mCards);
		mCardScrollView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				Card card = (Card) adapter.getItem(arg2);
				itemSelected = SqlHelper.getInstance(getApplicationContext())
						.findExactRememberItem(card.getText().toString());
				openOptionsMenu();

			}
		});

		mCardScrollView.setAdapter(adapter);
		mCardScrollView.activate();
		setContentView(mCardScrollView);
	}

	private void showSucessDeleteCard(RememberItem item) {
		Card card = new Card(this);
		card.setImageLayout(ImageLayout.FULL);
		card.addImage(R.drawable.deleted);
		setContentView(card.getView());
		Handler handler = new Handler(this);
		handler.sendEmptyMessageDelayed(DELETE, 3000);
	}

	@Override
	public boolean handleMessage(Message msg) {
		if(msg.what==FAILURE){
			finish();
		}
		if(msg.what==DELETE){
			finish();
		}
		
		return false;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.saveditemmenu, menu);
		return true;
	}
	
	private void showEmptyCard(){
		setContentView(R.layout.card_full_image);
		mCardTitle = (TextView) findViewById(R.id.card_title);
		mCardSubTitle = (TextView) findViewById(R.id.card_subtitle);
		mCardImage = (ImageView) findViewById(R.id.card_image);
		mCardImage.setScaleType(ScaleType.CENTER_INSIDE);	
		mCardTitle.setText(R.string.empty_card_message);
		mCardSubTitle.setText(R.string.empty_card_subtitle);
		mCardImage.setImageResource(R.drawable.finditlogobg);
		mCardTitle.setGravity(Gravity.CENTER);
		mCardSubTitle.setGravity(Gravity.CENTER);
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem menuitem) {
		if (itemSelected == null) {
			return super.onOptionsItemSelected(menuitem);
		}
		switch (menuitem.getItemId()) {
		case R.id.menu_getdirections:
			RememberUtils.launchGoogleMap(this,itemSelected);
			return true;
		case R.id.menu_delete:
			boolean result = SqlHelper.getInstance(getApplication())
					.deleteRememberItem(itemSelected);
			if (result) {
				showSucessDeleteCard(itemSelected);
			}
			return true;
		case R.id.menu_cancel:
			closeOptionsMenu();
			return true;
		default:
			return super.onOptionsItemSelected(menuitem);
		}
	}
}