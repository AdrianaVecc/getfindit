package com.adrianavecchioli.findit;

import android.app.Application;

import com.adrianavecchioli.findit.util.ParseUtil;
import com.parse.Parse;

public class FindItApplication extends Application {
	@Override
	public void onCreate() {
		super.onCreate();
		Parse.initialize(this, ParseUtil.APP_ID,ParseUtil.CLIENT_KEY);
	}
}
