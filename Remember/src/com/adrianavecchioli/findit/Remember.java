package com.adrianavecchioli.findit;

import java.io.File;
import java.util.ArrayList;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Bundle;
import android.os.FileObserver;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.provider.MediaStore;
import android.speech.RecognizerIntent;
import android.text.format.DateUtils;
import android.view.KeyEvent;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;

import com.adrianavecchioli.findit.db.SqlHelper;
import com.adrianavecchioli.findit.domain.RememberItem;
import com.adrianavecchioli.findit.provider.AndroidLocationProvider;
import com.adrianavecchioli.findit.provider.LocationChangedListener;
import com.adrianavecchioli.findit.util.IDGenerator;
import com.adrianavecchioli.findit.util.RememberUtils;
import com.adrianavecchioli.findit.util.ReverseGeocodingTask;
import com.google.android.glass.app.Card;
import com.google.android.glass.app.Card.ImageLayout;
import com.google.android.glass.media.CameraManager;

public class Remember extends BaseActivity implements Callback{
	
	private static final int TAKE_PICTURE_REQUEST_CODE = 1;
	private RememberItem item;
	private static final int CODE_GPS_ERROR=1212;
	private static final int CODE_SUCCESS=1009;
	private static final long GPS_TIMEOUT=DateUtils.MINUTE_IN_MILLIS;
	
	private TextView mCardTitle;
	private TextView mCardSubTitle;
	private ImageView mCardImage;



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		showTakeAPictureView();
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		

	}
	private void showTakeAPictureView() {
				
		setContentView(R.layout.card_full_image);
		mCardTitle = (TextView) findViewById(R.id.card_title);
		mCardSubTitle = (TextView) findViewById(R.id.card_subtitle);
		mCardImage = (ImageView) findViewById(R.id.card_image);
		mCardImage.setScaleType(ScaleType.CENTER);
		mCardTitle.setText(R.string.tap_to_take_a_photo);
		mCardSubTitle.setText("");
		mCardImage.setImageResource(R.drawable.taptotakeapicture2);

	}

	@Override
	public boolean onKeyDown(int keycode, KeyEvent event) {
		if (keycode == KeyEvent.KEYCODE_DPAD_CENTER && item==null) {
			takePicture();
			return true;
		} else {
			return false;
		}
	}

	private void takePicture() {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		startActivityForResult(intent, TAKE_PICTURE_REQUEST_CODE);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == TAKE_PICTURE_REQUEST_CODE && resultCode == RESULT_OK) {
			String picturePath = data
					.getStringExtra(CameraManager.EXTRA_THUMBNAIL_FILE_PATH);
			processPictureWhenReady(picturePath);
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	private void processPictureWhenReady(final String picturePath) {
		final File pictureFile = new File(picturePath);

		if (pictureFile.exists()) {
			processFile(pictureFile);
		} else {

			final File parentDirectory = pictureFile.getParentFile();
			FileObserver observer = new FileObserver(parentDirectory.getPath()) {
				private boolean isFileWritten;

				@Override
				public void onEvent(int event, String path) {
					if (!isFileWritten) {
						File affectedFile = new File(parentDirectory, path);
						isFileWritten = (event == FileObserver.CLOSE_WRITE && affectedFile
								.equals(pictureFile));

						if (isFileWritten) {
							stopWatching();
							runOnUiThread(new Runnable() {
								@Override
								public void run() {
									processPictureWhenReady(picturePath);
								}
							});
						}
					}
				}
			};
			observer.startWatching();
		}
	}

	private void processFile(File pictureFile) {
		 item=new RememberItem();
		item.setTag(tag);
		
		String imagePath=pictureFile.getAbsolutePath();
		item.setImagePath(imagePath);
		
		long addedDate=System.currentTimeMillis();
		item.setAddedDate(addedDate);
		item.setId(IDGenerator.generatedUniqueId());

		if(lastLocation!=null){
			item.setLocation(lastLocation);
			SqlHelper.getInstance(this).saveRememberItem(item);
			showSucessCard(item);
			sendSuccessMessage(3000);
		}else{
			showWaitingLocationCard();
			Handler handler=new Handler(this);
			Message message=Message.obtain();
			message.what=CODE_GPS_ERROR;
			handler.sendMessageDelayed(message, GPS_TIMEOUT);
		}
		
	}
	private void showSucessCard(RememberItem item){
		Card card = new Card(this);
		card.setText(R.string.object_saved);
		card.setImageLayout(ImageLayout.LEFT);
		card.addImage(BitmapFactory.decodeFile(item.getImagePath()));
		card.setFootnote(item.getTag());
		setContentView(card.getView());
	}
	
	@Override
	public boolean handleMessage(Message msg) {
		if (msg.what==CODE_GPS_ERROR && item!=null){
			item.setLocation(RememberUtils.getDefaultLocation());
			SqlHelper.getInstance(this).saveRememberItem(item);
			showSucessCard(item);
			sendSuccessMessage(1000);
		}
		if (msg.what==CODE_SUCCESS){
			finish();	
		}
		return false;
	}
	@Override
	public void onLocationReceived(Location location) {
		super.onLocationReceived(location);
		if(item!=null){
			item.setLocation(location);
			SqlHelper.getInstance(this).saveRememberItem(item);
			showSucessCard(item);
			sendSuccessMessage(1000);
		}
		
	}
	private void sendSuccessMessage(int time) {
		Handler handler=new Handler(this);
		handler.removeCallbacksAndMessages(null);
		Message message=Message.obtain();
		message.what=CODE_SUCCESS;
		handler.sendMessageDelayed(message, time);
		if(item.getLocation().getLatitude()!=-1 || item.getLocation().getLongitude()!=-1){
		  new ReverseGeocodingTask(this,this.item.getId(),item.getLocation()).execute();
		} 
		this.item=null;
	}
	private void showWaitingLocationCard(){
		Card card = new Card(this);
		card.setText(R.string.waiting_best_location);
		card.setImageLayout(ImageLayout.LEFT);
		card.addImage(BitmapFactory.decodeFile(item.getImagePath()));
		card.setFootnote(item.getTag());
		setContentView(card.getView());
	}
	
}
