package com.adrianavecchioli.findit.domain;

import com.parse.ParseObject;

public class RememberUser {

	public static final String KEY_EMAIL="email";
	
	
	private String email;
	
	
	
	public ParseObject buildParseObject(){
		ParseObject object = new ParseObject(RememberItem.class.getSimpleName());
		object.put(KEY_EMAIL,email);
		object.setObjectId(email);
		return object;
	}



	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}
	
}
