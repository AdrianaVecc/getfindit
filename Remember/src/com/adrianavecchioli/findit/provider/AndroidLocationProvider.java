package com.adrianavecchioli.findit.provider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.util.Log;


public class AndroidLocationProvider implements LocationListener {

	
	private LocationChangedListener changedListener;
	private LocationManager locationManager;
	private Location lastLocation=null;
	private static final long LOCATION_EXPIRATION_DELAY=5*DateUtils.MINUTE_IN_MILLIS;
	
	public AndroidLocationProvider(Context context,LocationChangedListener locationChangedListener) {
		locationManager=(LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
		this.changedListener=locationChangedListener;
		this.lastLocation=getLastLocationInternal();
	}
	

	public void startLocationService(){
		 locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
		 List<String> providers=locationManager.getAllProviders();
		 for(String provider :providers){
			 if(provider!=null){
				 locationManager.requestLocationUpdates(provider, 0, 0, this);	 
			 }
		 }
	}
	public void stopLocationService(){
		locationManager.removeUpdates(this);
	}
	
	
	@Override
	public void onLocationChanged(Location location) {
		this.lastLocation=location;
		changedListener.onLocationReceived(location);
	}

	@Override
	public void onProviderDisabled(String provider) {
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}
	
	 private  Location getLastLocationInternal() {
	      
	      Criteria criteria = new Criteria();
	      criteria.setAccuracy(Criteria.NO_REQUIREMENT);
	      List<String> providers = locationManager.getProviders(criteria, true);
	      List<Location> locations = new ArrayList<Location>();
	      for (String provider : providers) {
	           Location location = locationManager.getLastKnownLocation(provider);
	           if (location != null) {
	               locations.add(location);
	           }
	      }
	      Collections.sort(locations, new Comparator<Location>() {
	          @Override
	          public int compare(Location location, Location location2) {
	        	  if(System.currentTimeMillis()-location.getTime()>LOCATION_EXPIRATION_DELAY){
	        		  return -1;
	        	  }
	        	  if(System.currentTimeMillis()-location2.getTime()>LOCATION_EXPIRATION_DELAY){
	        		  return 1;
	        	  }
	              return (int) (location.getAccuracy() - location2.getAccuracy());
	          }
	      });
	      if (locations.size() > 0) {
	          return locations.get(0);
	      }
	      return null;
	 }


	public Location getLastLocation() {
		if(this.lastLocation==null){
			this.lastLocation=getLastLocationInternal();
		}
		return lastLocation;
	}
	 

	
	
}
