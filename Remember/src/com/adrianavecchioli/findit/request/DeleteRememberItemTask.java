package com.adrianavecchioli.findit.request;

import java.util.List;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.adrianavecchioli.findit.db.SqlHelper;
import com.adrianavecchioli.findit.util.UploadFormatBuilder;

public class DeleteRememberItemTask extends
		AsyncTask<String, Integer, Integer> {

	private static final int SUCCESS = 1;
	private static final int FAILURE= 0;
	
	private List<String> rememberItemIds=null;
	private Context context;
	
	public DeleteRememberItemTask(Context context,List<String> rememberItemIds){
		this.rememberItemIds=rememberItemIds;
		this.context=context;
	}
	@Override
	protected Integer doInBackground(String... params) {
		try{
			UploadFormatBuilder.delete(rememberItemIds);
			Log.i("DELETE OK ","OK");
		}catch(Exception exception){
			Log.i("REMEMBER", "DELETE ERROR ");
			return FAILURE;
		}
		return SUCCESS;
	}
	@Override
	protected void onPostExecute(Integer result) {
		super.onPostExecute(result);
		if(result!=null && result==SUCCESS){
			SqlHelper.getInstance(context).clearDeletedIds();
		}
	}

}
