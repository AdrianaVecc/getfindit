package com.adrianavecchioli.findit.request;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.adrianavecchioli.findit.db.SqlHelper;
import com.adrianavecchioli.findit.domain.RememberItem;
import com.adrianavecchioli.findit.domain.RememberUser;
import com.adrianavecchioli.findit.util.UploadFormatBuilder;

public class UploadRememberItemTask extends
		AsyncTask<String, Integer, Integer> {

	private static final int SUCCESS = 1;
	private static final int FAILURE= 0;
	
	private Context context;
	private RememberUser user;
	
	
	public UploadRememberItemTask(Context context,RememberUser user) {
		super();
		this.context = context;
		this.user=user;
	}
	@Override
	protected Integer doInBackground(String... params) {
		try{
			List<String> itemIdsInServer=UploadFormatBuilder.list(user);
			Log.i("REMEMBER", "IN SERVER "+itemIdsInServer);
			List<RememberItem> itemObjectInLocal=SqlHelper.getInstance(context).findAllRememberItem();
			List<RememberItem> itemObjectToUploads=new ArrayList<RememberItem>();
			Log.i("REMEMBER", "TO BE UPLOADED "+itemObjectInLocal.size() );
			for(RememberItem item:itemObjectInLocal){
					if(!itemIdsInServer.contains(item.getId())){
						itemObjectToUploads.add(item);
					}
			}
			UploadFormatBuilder.upload(user,itemObjectToUploads);
			Log.i("REMEMBER", "UPLOADING OK ");
		}catch(Exception exception){
			Log.i("REMEMBER", "UPLOADING FAIL "+exception.toString());
			exception.printStackTrace();
			return FAILURE;
		}
		return SUCCESS;
	}

	@Override
	protected void onPostExecute(Integer result) {
		super.onPostExecute(result);
		if(result!=null && result==SUCCESS){
			SqlHelper.getInstance(context).clearUploaded();
		}
	}

}
