package com.adrianavecchioli.findit.service;

import java.util.List;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.adrianavecchioli.findit.db.SqlHelper;
import com.adrianavecchioli.findit.domain.RememberUser;
import com.adrianavecchioli.findit.request.DeleteRememberItemTask;
import com.adrianavecchioli.findit.request.UploadRememberItemTask;
import com.adrianavecchioli.findit.util.EmailFinder;

public class SynchronizedItemsService extends Service  {

	
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		RememberUser user=EmailFinder.buildUser(this);
		new UploadRememberItemTask(this, user).execute("");
		List<String>idOfRememberItemToBedeleted=SqlHelper.getInstance(this).findDeletedRememberItemId();
		new DeleteRememberItemTask(this, idOfRememberItemToBedeleted).execute("");
		return super.onStartCommand(intent, flags, startId);
	}
}
