package com.adrianavecchioli.findit.util;

import com.adrianavecchioli.findit.domain.RememberUser;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;

public class EmailFinder {

	public static String findUserAccount(Context context) {
		try {

			Account[] emailAccounts = AccountManager.get(context).getAccountsByType(
					"com.google");
			for (Account account : emailAccounts) {
				if (account.name != null) {
					return account.name;
				}

			}
			Account[] accountsOther = AccountManager.get(context).getAccounts();
			for (Account account : accountsOther) {
				if (account.name != null) {
					return account.name;
				}

			}
		} catch (Exception e) {

		}
		return null;
	}

	public static RememberUser buildUser(Context context) {
		RememberUser user =new RememberUser();
		String email=findUserAccount(context);
		user.setEmail(email);
		return user;
	}
}
