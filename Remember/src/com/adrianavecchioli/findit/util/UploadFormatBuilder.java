package com.adrianavecchioli.findit.util;

import java.util.ArrayList;
import java.util.List;

import android.graphics.Bitmap;

import com.adrianavecchioli.findit.domain.RememberItem;
import com.adrianavecchioli.findit.domain.RememberUser;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;

public class UploadFormatBuilder {

	public static void upload(final RememberUser user,List<RememberItem> itemsToUpload) throws ParseException {
		if(itemsToUpload==null){
			return ;
		}
		List<ParseObject> objects=new ArrayList<ParseObject>();
		for(RememberItem item:itemsToUpload){
			uploadItem(user, item);
		}
		if(objects.isEmpty()){
			return;
		}

	}
	
	public static void uploadItem( final RememberUser user, final RememberItem item){
		try {
			Bitmap bitmap=RememberUtils.getBitmap(item.getImagePath());
			byte[] data=RememberUtils.convertBitmapToByte(bitmap);
			final ParseFile file = new ParseFile(item.getId()+".png", data);
			file.save();
			ParseObject object=item.buildParseObject(user);
			ParseGeoPoint geoPoint=new ParseGeoPoint(item.getLocation().getLatitude(), item.getLocation().getLongitude());
			object.put(RememberItem.KEY_IMAGE, file);
			object.put(RememberItem.GEOPOINT, geoPoint);
			object.save();
		}catch(Exception exception){
			exception.printStackTrace();
		}
		
		
	}

	public static List<String> list(RememberUser user) throws ParseException {
		ParseQuery<ParseObject> query = ParseQuery.getQuery(RememberItem.class
				.getSimpleName());
		List<ParseObject> resultFromServer = query.whereEqualTo(RememberItem.KEY_ID_USER, user.getEmail()).find();
		List<String> list = new ArrayList<String>();
		for (ParseObject parseObject : resultFromServer) {
			list.add(parseObject.get(RememberItem.KEY_ID).toString());
		}
		return list;

	}

	public static void delete(List<String> rememberItemIds)
			throws ParseException {
		ParseQuery<ParseObject> query = ParseQuery.getQuery(RememberItem.class
				.getSimpleName());
		List<ParseObject> parseObjectsToDeletes = query.whereContainedIn(
				RememberItem.KEY_ID, rememberItemIds).find();
		for (ParseObject object : parseObjectsToDeletes) {
			if (object != null) {
				object.delete();
			}
		}
	}
}
